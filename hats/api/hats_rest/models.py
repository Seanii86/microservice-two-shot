from django.db import models
from django.urls import reverse

class locationVO(models.Model):
    closet_name = models.CharField(max_length=200, unique=True)
    section_number = models.PositiveSmallIntegerField(default=1)
    shelf_number = models.PositiveSmallIntegerField(default=1)
    import_href = models.CharField(max_length=200, unique=True, null=True)



class Hats(models.Model):
    fabric = models.CharField(max_length=40)
    style_name = models.CharField(max_length=40)
    color = models.CharField(max_length=20)
    picture_url = models.URLField()

    location = models.ForeignKey(
        locationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.style_name}"

