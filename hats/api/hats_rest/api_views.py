from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import locationVO, Hats

class LocationVOEncoder(ModelEncoder):
    model = locationVO
    properties = [
        "import_href",
        "closet_name",
        "shelf_number",
        "section_number",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }

class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            href = content['location']
            location = locationVO.objects.get(import_href=href)
            content['location'] = location
        except locationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location does not exist- invalid location id"},
                status=400,
            )
        Hat = Hats.objects.create(**content)
        return JsonResponse(
            Hat,
            encoder=HatDetailEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def api_show_hats(request, pk):
    if request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})