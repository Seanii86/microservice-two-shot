# Wardrobify

Team:

* Person 1 - Sean - Hats
* Person 2 - Gary - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

Write basic models, create views for creating, updated and deleting hats, add form and list data to react app, configure nav.